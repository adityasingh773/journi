export type TCountry = {
  name: string;
  distance: number;
  countryCode: string;
};

export type TCountryMetadata = {
  sovereignt: string;
  lat: number;
  lng: number;
  iso_a2: string;
};

export type TResponse = {
  countries: TCountry[];
};
