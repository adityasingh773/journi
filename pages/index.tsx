import { useCallback, useEffect, useState } from "react";
import { TCountry } from "../@types/types";
import Dropdown from "../components/Dropdown/Dropdown";
import Input from "../components/Input/Input";
import useLazyRequest from "../hooks/useLazyRequest";
import RestAPIList from "../services/RestAPIList";
import styles from "../styles/Home.module.css";
import Head from "next/head";
import InfoBox from "../components/InfoBox/InfoBox";

const Home = () => {
  const [query, setQuery] = useState<string>("");

  const [searchedCountries, setSearchedCountries] = useState<TCountry[]>([]);

  const [selectedCountryDetails, setSelectedCountryDetails] = useState<
    TCountry | undefined
  >(undefined);

  // default lat and lon initialisation
  const [currentLatLon, setCurrentLatLon] = useState<{
    lat: number;
    lon: number;
  } | null>(null);

  // api call for default lat and lon
  const { call: $getCurrentLatLon } = useLazyRequest(
    RestAPIList.countries.getLatAndLong
  );

  // api call to get countries name and distance
  const { call: getCountries } = useLazyRequest(
    RestAPIList.countries.getNameAndDistance
  );

  const getCurrentLatLon = async () => {
    try {
      const currentLocationData = await $getCurrentLatLon();

      if (currentLocationData.status !== "success") {
        alert("Failed to get current Coordinates");
      }

      // set default lat and lon if the query is successful
      const { lat, lon } = currentLocationData;
      setCurrentLatLon({
        lat,
        lon,
      });
    } catch (err) {
      console.log(err);
      alert("Something went wrong");
    }
  };

  // Filter countries based on input query
  // using usecallback to make sure the function is updated when the dependenices are updated
  const getFilteredCountries = useCallback(async () => {
    if (!query) {
      setSearchedCountries([]);
      return;
    }

    // wait for default lat and lon to be set
    if (!currentLatLon) return;

    // once default lat and lon is set, countries can be searched for
    const { lat, lon } = currentLatLon;
    try {
      const result = await getCountries(query, lat, lon);

      setSearchedCountries(result.countries);
    } catch (err) {
      alert("Failed to get data");
    }
  }, [query, currentLatLon]);

  const handleOnSelect = (country: TCountry) => {
    const { name, distance, countryCode } = country;
    setSelectedCountryDetails({
      name,
      distance,
      countryCode,
    });
  };

  // set default lat and lon upon component mount
  useEffect(() => {
    getCurrentLatLon();
  }, []);

  // update countries list if the query is updated
  useEffect(() => {
    getFilteredCountries();
  }, [getFilteredCountries]);

  useEffect(() => {
    // hide info box when query is blank
    if (query === "") {
      setSelectedCountryDetails(undefined);
    }
  }, [query]);

  const renderApp = () => {
    if (!currentLatLon) {
      return <div>Loading your location</div>;
    }
    return (
      <div>
        <div className={styles.titleContainer}>
          <span className={styles.title}>Find the closest country</span>
        </div>
        <div>
          <Input value={query} onChange={setQuery} />
        </div>
        <div>
          <Dropdown
            onSelect={handleOnSelect}
            countries={searchedCountries}
            currentQuery={query}
          />
        </div>
        <div>
          <InfoBox
            shouldShow={selectedCountryDetails ? true : false}
            metaData={selectedCountryDetails || undefined}
          />
        </div>
      </div>
    );
  };

  return (
    <div className={styles.root}>
      <Head>
        <title>Find the closest country</title>
      </Head>
      <div className={styles.container}>{renderApp()}</div>
    </div>
  );
};

export default Home;
