import { NextApiRequest, NextApiResponse } from "next";
import path from "path";
import { promises as fs } from "fs";
import { TCountryMetadata, TResponse } from "../../@types/types";

const getCountryList = async (): Promise<TCountryMetadata[]> => {
  // find the absolute path of the json directory
  const jsonDirectory = path.join(process.cwd(), "json");
  // read the json data file countries.json
  const list = await fs.readFile(jsonDirectory + "/countries.json", "utf8");
  return JSON.parse(list).countries ?? [];
};

function getDistance(lat1: number, lon1: number, lat2: number, lon2: number) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}
function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}

export default async function getCountries(
  req: NextApiRequest,
  res: NextApiResponse<TResponse>
) {
  const { query, lat, lon } = req.query;
  const countriesData = await getCountryList();

  const countries = countriesData

    .filter((country) => {
      // ^ for matching first character starting with query
      // i is for case insensitive query
      if (country.sovereignt.match(new RegExp(`^${query as string}`, "i")))
        return true;
      return false;
    })
    .map((e) => ({
      // store name and distance for readability in additional key in the object
      name: e.sovereignt,
      distance: getDistance(
        parseFloat(lat as string),
        parseFloat(lon as string),
        e.lat,
        e.lng
      ),
      countryCode: e.iso_a2,
    }))
    .sort((countryA, countryB) => {
      // sort countries based on the closest distance
      return countryA.distance - countryB.distance;
    });

  res.send({
    countries,
  });
}
