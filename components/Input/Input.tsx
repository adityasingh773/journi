import React, { FC, ChangeEvent, useState, useEffect } from "react";
import styles from "./Input.module.css";

type TProps = {
  value: string;
  onChange: (value: string) => void;
};

const Input: FC<TProps> = ({ value, onChange }) => {
  const [$value, setValue] = useState<string>(value);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  useEffect(() => {
    onChange($value);
  }, [$value]);

  return (
    <div className={styles.container}>
      <input
        className={styles.input}
        type="text"
        value={$value}
        onChange={handleChange}
      />
    </div>
  );
};

export default Input;
