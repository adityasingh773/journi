import React, { FC } from "react";
import { TCountry } from "../../@types/types";
import styles from "./InfoBox.module.css";

type TProps = {
  shouldShow: boolean;
  metaData: TCountry | undefined;
};

const InfoBox: FC<TProps> = ({ shouldShow, metaData }) => {
  if (!shouldShow) return <></>;

  return (
    <div className={styles.infoBox}>
      The country {metaData?.name} is {metaData?.distance.toFixed(2)} kms away
      from you and it's country code is {metaData?.countryCode}
    </div>
  );
};

export default InfoBox;
