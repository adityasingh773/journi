import React, { FC } from "react";
import { TCountry } from "../../@types/types";
import styles from "./Dropdown.module.css";

type TProps = {
  currentQuery: string;
  countries: TCountry[];
  onSelect: (country: TCountry) => void;
};

const Dropdown: FC<TProps> = ({ countries, currentQuery, onSelect }) => {
  const renderCountry = () => {
    return countries.map((country, i) => (
      <button
        onClick={() => onSelect(country)}
        key={`${country.name}-${i}`}
        className={styles.result}
      >
        {/* mark the current queried string as bold */}
        <b>{currentQuery}</b>
        {country.name.replace(new RegExp(`^${currentQuery}`, "i"), "")}
      </button>
    ));
  };

  return <div className={styles.container}>{renderCountry()}</div>;
};

export default Dropdown;
