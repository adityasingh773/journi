const RestAPIList = {
  countries: {
    getNameAndDistance: async (query: string, lat: number, lon: number) => {
      const res = await fetch(
        `/api/getCountries?query=${query}&lat=${lat}&lon=${lon}`
      ).then((res) => res.json());
      return res;
    },
    getLatAndLong: async () => {
      const res = await fetch("http://ip-api.com/json/").then((res) =>
        res.json()
      );
      return res;
    },
  },
};

export default RestAPIList;
